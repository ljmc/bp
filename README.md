# bp

bioinformatics problems

`go` [![pipeline status](https://gitlab.com/ljmc/bp/badges/main/pipeline.svg)](https://gitlab.com/ljmc/bp/-/commits/main) [![coverage report](https://gitlab.com/ljmc/bp/badges/main/coverage.svg)](https://gitlab.com/ljmc/bp/-/commits/main)

#### Problems:

Legend:
- `PUB` published
- `WIP` work in progress
- `PRV` private (working version, just not yet published)

| Problem | `go`  | `f90` | `ijs` |
| :-:     | :-:   | :-:   | :-:   |
| `DNA`   | `PUB` | `PRV` | `PUB` |
| `RNA`   | `PUB` | `PRV` | `PUB` |
| `REVC`  | `PUB` | `PRV` | `PUB` |
| `FIB`   | `PUB` | `PRV` |       |
| `GC`    | `PUB` | `PRV` | `PUB` |
| `HAMM`  | `PUB` | `PRV` | `PUB` |
| `IPRB`  | `PUB` | `PRV` | `PUB` |
| `PROT`  | `PUB` | `PRV` | `PUB` |
| `SUBS`  | `PUB` | `PRV` |       |
| `CONS`  | `PUB` |       |       |
| `FIBD`  |       |       |       |
| `GRPH`  | `PUB` | `PRV` | `WIP` |
| `IEV`   | `PUB` |       | `PUB` |
| `LCSM`  | `PUB` |       |       |
| `LIA`   | `PUB` |       |       |
| `MPRT`  | `PUB` |       |       |
| `MRNA`  | `PUB` | `PRV` |       |
| `ORF`   | `PUB` |       |       |
| `PERM`  | `PUB` |       |       |
| `PRTM`  | `PUB` | `PRV` |       |
| `REVP`  | `PUB` |       |       |
| `SPLC`  | `PUB` |       |       |
| `LEXF`  | `PRV` |       |       |
| `LGIS`  |       |       |       |
| `LONG`  | `PUB` |       |       |
| `PMCH`  | `PUB` |       |       |
| `PPER`  |       |       |       |
| `PROB`  | `PRV` |       |       |
| `SIGN`  | `PUB` |       |       |
| `SSEQ`  | `PUB` |       |       |
| `TRAN`  | `PUB` |       |       |
| `TREE`  |       |       |       |
| `CAT`   |       |       |       |
| `CORR`  | `PRV` |       |       |
| `INOD`  |       |       |       |
| `KMER`  | `PRV` |       |       |
| `KMP`   |       |       |       |
| `LCSQ`  |       |       |       |
| `LEXV`  |       |       |       |
| `MMCH`  |       |       |       |
| `PDST`  |       |       |       |
| `REAR`  |       |       |       |
| `RSTR`  |       |       |       |
| `SSET`  |       |       |       |
| `ASPC`  |       |       |       |
| `EDIT`  |       |       |       |
| `EVAL`  |       |       |       |
| `MOTZ`  |       |       |       |
| `NWCK`  |       |       |       |
| `SCSP`  |       |       |       |
| `SETO`  |       |       |       |
| `SORT`  |       |       |       |
| `SPEC`  |       |       |       |
| `TRIE`  |       |       |       |
| `CONV`  |       |       |       |
| `CTBL`  |       |       |       |
| `DBRU`  |       |       |       |
| `EDTA`  |       |       |       |
| `FULL`  |       |       |       |
| `INDC`  |       |       |       |
| `ITWV`  |       |       |       |
| `LREP`  |       |       |       |
| `NKEW`  |       |       |       |
| `RNAS`  |       |       |       |
| `AFRQ`  |       |       |       |
| `CSTR`  |       |       |       |
| `CTEA`  |       |       |       |
| `CUNR`  |       |       |       |
| `GLOB`  |       |       |       |
| `PCOV`  |       |       |       |
| `PRSM`  |       |       |       |
| `QRT`   |       |       |       |
| `SGRA`  |       |       |       |
| `SUFF`  |       |       |       |
| `CHBP`  |       |       |       |
| `CNTQ`  |       |       |       |
| `EUBT`  |       |       |       |
| `GASM`  |       |       |       |
| `GCON`  |       |       |       |
| `LING`  |       |       |       |
| `LOCA`  |       |       |       |
| `MEND`  |       |       |       |
| `MGAP`  |       |       |       |
| `MREP`  |       |       |       |
| `MULT`  |       |       |       |
| `PDPL`  |       |       |       |
| `ROOT`  |       |       |       |
| `SEXL`  |       |       |       |
| `SPTD`  |       |       |       |
| `WFMD`  |       |       |       |
| `ALPH`  |       |       |       |
| `ASMQ`  |       |       |       |
| `CSET`  |       |       |       |
| `EBIN`  |       |       |       |
| `FOUN`  |       |       |       |
| `GAFF`  |       |       |       |
| `GREP`  |       |       |       |
| `OAP`   |       |       |       |
| `QRTD`  |       |       |       |
| `SIMS`  |       |       |       |
| `SMGB`  |       |       |       |
| `KSIM`  |       |       |       |
| `LAFF`  |       |       |       |
| `OSYM`  |       |       |       |
| `RSUB`  |       |       |       |

<!-- vim:set tw=0: -->
