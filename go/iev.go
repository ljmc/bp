package bp

func ExpectedOffspringDominantPhenotype(a, b, c, d, e, f int64) float64 {

	var p float64

	n := 2.0

	p = n * (float64(a) + float64(b) + float64(c) + 0.75*float64(d) + 0.5*float64(e))

	return p
}
