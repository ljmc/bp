package bp

import "testing"

func TestSignPermutations(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		in   int
		out  [][]int
	}{
		{"2", 2, [][]int{
			{-1, -1},
			{-1, 1},
			{1, -1},
			{1, 1},
		}},
		{"3", 3, [][]int{
			{-1, -1, -1},
			{-1, -1, 1},
			{-1, 1, -1},
			{-1, 1, 1},
			{1, -1, -1},
			{1, -1, 1},
			{1, 1, -1},
			{1, 1, 1},
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			res := SignPermutations(test.in)
			for i, v := range res {
				for j, w := range v {
					if w != test.out[i][j] {
						t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
					}
				}
			}
		})
	}
}

func TestHeapAlgorithmSigned(t *testing.T) {
	argument := []int{1, 2}
	expected := [][]int{
		{-1, -2},
		{-1, 2},
		{1, -2},
		{1, 2},
		{-2, -1},
		{-2, 1},
		{2, -1},
		{2, 1},
	}
	actual := HeapAlgorithmSigned(argument)

	for i, v := range actual {
		for j, w := range v {
			if w != expected[i][j] {
				t.Errorf("Test failed, expected: '%d', got: '%d'", expected, actual)
			}
		}
	}
}
