package bp

func ReverseComplement(sequence string) string {

	var rcSequence string

	for i := len(sequence) - 1; i >= 0; i-- {
		switch sequence[i] {
		case 'A':
			rcSequence += "T"
		case 'C':
			rcSequence += "G"
		case 'G':
			rcSequence += "C"
		case 'T':
			rcSequence += "A"
		}
	}

	return rcSequence
}
