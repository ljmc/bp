package bp

import "strings"

func FindMotifs(sequence, motif string) []int {

	var indexes []int
	var i int

	for strings.Contains(sequence[i:], motif) {
		if indexes == nil {
			i = strings.Index(sequence[i:], motif) + 1
		} else {
			i = strings.Index(sequence[i:], motif) + 1 + indexes[len(indexes)-1]
		}
		indexes = append(indexes, i)
	}

	return indexes
}
