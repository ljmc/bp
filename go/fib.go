package bp

func Fibonacci(n, k int) int {

	prev, curr := 0, 1

	for i := 1; i < n; i++ {
		prev, curr = curr, k*prev+curr
	}

	return curr
}
