package bp

import (
	"sort"
	"testing"
)

func TestFindEdges(t *testing.T) {
	argument := map[string]string{
		"Rosalind_0498": "AAATAAA",
		"Rosalind_2391": "AAATTTT",
		"Rosalind_2323": "TTTTCCC",
		"Rosalind_0442": "AAATCCC",
		"Rosalind_5013": "GGGTGGG",
	}
	expected := []string{
		"Rosalind_0498 Rosalind_0442",
		"Rosalind_0498 Rosalind_2391",
		"Rosalind_2391 Rosalind_2323",
	}
	actual := FindEdges(argument, 3)
	sort.Strings(actual)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%s', got:  '%s'", expected[i], actual[i])
		}
	}
}
