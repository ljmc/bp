package bp

import (
	"fmt"
	"math/big"
	"strings"
)

func CountRNANucleotides(rnaSequence string) [4]int {

	var counts [4]int

	nucleotides := [4]string{"A", "C", "G", "U"}

	for i, v := range nucleotides {
		counts[i] = strings.Count(rnaSequence, v)
	}

	return counts
}

func CountPerfectMatchings(rnaSequence string) (*big.Int, error) {

	var matchingsAU, matchingsCG *big.Int = big.NewInt(1), big.NewInt(1)

	counts := CountRNANucleotides(rnaSequence)

	if counts[0] != counts[3] || counts[1] != counts[2] {
		return big.NewInt(0), fmt.Errorf(
			"Sequence does not have perfect matchings A=%d, U=%d and C=%d, G=%d)",
			counts[0], counts[3], counts[1], counts[2],
		)
	}

	matchingsAU.MulRange(matchingsAU.Int64(), int64(counts[0]))
	matchingsCG.MulRange(matchingsCG.Int64(), int64(counts[1]))

	return matchingsAU.Mul(matchingsAU, matchingsCG), nil
}
