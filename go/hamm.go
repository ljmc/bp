package bp

import "fmt"

func HammingDistance(seqA, seqB []byte) (int, error) {

	distance := 0

	if len(seqA) != len(seqB) {
		return 0, fmt.Errorf("Please input sequences of identical length")
	}

	for i := 0; i < len(seqA); i++ {
		if seqA[i] != seqB[i] {
			distance++
		}
	}

	return distance, nil
}
