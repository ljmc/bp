package bp

import "testing"

func TestExpectedOffspringDominantPhenotype(t *testing.T) {
	var argumentA, argumentB, argumentC, argumentD, argumentE, argumentF int64 = 1, 0, 0, 1, 0, 1
	expected := float64(3.5)
	actual := ExpectedOffspringDominantPhenotype(argumentA, argumentB, argumentC, argumentD, argumentE, argumentF)

	if !AlmostEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%f', got:  '%f'", expected, actual)
	}
}
