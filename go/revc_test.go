package bp

import "testing"

func TestReverseComplement(t *testing.T) {
	argument := "AAAACCCGGT"
	expected := "ACCGGGTTTT"
	actual := ReverseComplement(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
