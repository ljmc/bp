package bp

import "testing"

func TestFindMotifs(t *testing.T) {
	argumentA, argumentB := "GATATATGCATATACTT", "ATAT"
	expected := []int{2, 4, 10}
	actual := FindMotifs(argumentA, argumentB)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
		}
	}
}
