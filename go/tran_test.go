package bp

import "testing"

func TestMutationRatio(t *testing.T) {
	argumentSeqA := "GCAACGCACAACGAAAACCCTTAGGGACTGGATTATTTCGTGATCGTTGTAGTTATTGGAAGTACGGGCATCAACCCAGTT"
	argumentSeqB := "TTATCTGACAAAGAAAGCCGTCAACGGCTGGATAATTTCGCGATCGTGCTGGTTACTGGCGGTACGAGTGTTCCTTTGGGT"
	expected := float64(1.21428571429)
	actual, err := MutationRatio(argumentSeqA, argumentSeqB)

	if err != nil {
		t.Errorf("Test failed, error found '%s'", err)
	}

	if !AlmostEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%f', got:  '%f'", expected, actual)
	}
}
