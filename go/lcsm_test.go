package bp

import "testing"

func TestFindCommonMotif(t *testing.T) {
	argument := []string{"GATTACA", "TAGACCA", "ATACA"}
	expected := "CA"
	actual := FindCommonMotif(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
