package bp

func TranslateRNA(rnaSequence string) string {

	var peptideSequence string

	for i := 0; i+2 < len(rnaSequence); i += 3 {
		switch rnaSequence[i : i+3] {
		case "UUU", "UUC":
			peptideSequence += "F"
		case "UUA", "UUG", "CUU", "CUC", "CUA", "CUG":
			peptideSequence += "L"
		case "GCA", "GCC", "GCG", "GCU":
			peptideSequence += "A"
		case "AUU", "AUC", "AUA":
			peptideSequence += "I"
		case "AUG":
			peptideSequence += "M"
		case "GUU", "GUC", "GUA", "GUG":
			peptideSequence += "V"
		case "UCU", "UCC", "UCA", "UCG", "AGU", "AGC":
			peptideSequence += "S"
		case "CCU", "CCC", "CCA", "CCG":
			peptideSequence += "P"
		case "ACU", "ACC", "ACA", "ACG":
			peptideSequence += "T"
		case "UAU", "UAC":
			peptideSequence += "Y"
		case "CAU", "CAC":
			peptideSequence += "H"
		case "CAA", "CAG":
			peptideSequence += "Q"
		case "AAU", "AAC":
			peptideSequence += "N"
		case "AAA", "AAG":
			peptideSequence += "K"
		case "GAU", "GAC":
			peptideSequence += "D"
		case "GAA", "GAG":
			peptideSequence += "E"
		case "UGU", "UGC":
			peptideSequence += "C"
		case "UGG":
			peptideSequence += "W"
		case "CGU", "CGC", "CGA", "CGG", "AGA", "AGG":
			peptideSequence += "R"
		case "GGU", "GGC", "GGA", "GGG":
			peptideSequence += "G"
		case "UAA", "UAG", "UGA":
			break
		}
	}

	return peptideSequence
}

/*
Genetic Code:

UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G
*/
