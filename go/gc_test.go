package bp

import "testing"

func TestParseFasta(t *testing.T) {
	argument := []byte(">Rosalind_6404\nCCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC\nTCCCACTAATAATTCTGAGG\n" +
		">Rosalind_5959\nCCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT\nATATCCATTTGTCAGCAGACACGC\n" +
		">Rosalind_0808\nCCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC\nTGGGAACCTGCGGGCAGTAGGTGGAAT\n")
	expected := map[string]string{
		"Rosalind_6404": "CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCCTCCCACTAATAATTCTGAGG",
		"Rosalind_5959": "CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCTATATCCATTTGTCAGCAGACACGC",
		"Rosalind_0808": "CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT",
	}
	actual := ParseFasta(argument)

	for iA, vA := range actual {
		vE, ok := expected[iA]
		if !ok {
			t.Errorf("Test failed, index '%s', not found.", iA)
		} else {
			if vE != vA {
				t.Errorf("Test failed, expected: '%s', got:  '%s'", vE, vA)
			}
		}
	}
}

func TestGCContent(t *testing.T) {
	argument := "CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT"
	expected := float64(60.919540)
	actual := GCContent(argument)

	if !AlmostEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%f', got:  '%f'", expected, actual)
	}

}

func TestGCContentHighest(t *testing.T) {
	argument := map[string]string{
		"Rosalind_6404": "CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCCTCCCACTAATAATTCTGAGG",
		"Rosalind_5959": "CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCTATATCCATTTGTCAGCAGACACGC",
		"Rosalind_0808": "CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT",
	}
	expectedID := "Rosalind_0808"
	expectedGC := 60.919540
	actualID, actualGC := GCContentHighest(argument)

	if actualID != expectedID {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expectedID, actualID)
	}
	if !AlmostEqual(actualGC, expectedGC) {
		t.Errorf("Test failed, expected: '%f', got:  '%f'", expectedGC, actualGC)
	}
}
