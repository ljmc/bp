package bp

import "testing"

func TestRnaNumber(t *testing.T) {
	argument := "MA"
	expected := 12
	actual := NumberDifferentRNASequences(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
	}
}
