package bp

import "testing"

func TestTranscribe(t *testing.T) {
	argument := "GATGGAACTTGACTACGTAAATT"
	expected := "GAUGGAACUUGACUACGUAAAUU"
	actual := Transcribe(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
