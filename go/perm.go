package bp

func HeapAlgorithm(array []int) [][]int {

	var (
		permutations [][]int
		permutate    func(array []int, n int)
	)

	permutate = func(array []int, n int) {
		if n == 1 {
			tmp := make([]int, len(array))
			copy(tmp, array)
			permutations = append(permutations, tmp)
		} else {
			for i := 0; i < n; i++ {
				permutate(array, n-1)
				if n%2 == 0 {
					array[i], array[n-1] = array[n-1], array[i]
				} else {
					array[0], array[n-1] = array[n-1], array[0]
				}
			}
		}
	}

	permutate(array, len(array))

	return permutations
}

func Range(beg, end, step int) []int {

	var slice []int

	for n := beg; n < end; n += step {
		slice = append(slice, n)
	}

	return slice
}
