package bp

func PalindromesFindIndexes(sequence string, lengths []int) [][2]int {

	var palindromes [][2]int

	rcSequence := ReverseComplement(sequence)

	searchPalindrome := func(sequence, rcSequence string, length int) {
		sequenceLength := len(sequence)
		for i := 0; i <= sequenceLength-length; i++ {
			if sequence[i:i+length] == rcSequence[sequenceLength-i-length:sequenceLength-i] {
				palindromes = append(palindromes, [2]int{i + 1, length})
			}
		}
	}

	for _, length := range lengths {
		searchPalindrome(sequence, rcSequence, length)
	}

	return palindromes
}
