package bp

import (
	"strings"
)

func CountDNANucleotides(dnaSequence string) [4]int {

	var counts [4]int

	nucleotides := [4]string{"A", "C", "G", "T"}

	for i, v := range nucleotides {
		counts[i] = strings.Count(dnaSequence, v)
	}

	return counts
}
