package bp

import "testing"

func TestProbabilityOffspringDominantAllele(t *testing.T) {
	argumentHomR := 2.
	argumentHet := 2.
	argumentHomD := 2.
	expected := 0.78333
	actual := ProbabilityOffspringDominantAllele(argumentHomR, argumentHet, argumentHomD)

	if !AlmostEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%f', got:  '%f'", expected, actual)
	}
}
