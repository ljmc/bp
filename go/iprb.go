package bp

func ProbabilityOffspringDominantAllele(HomR, Het, HomD float64) float64 {

	t := HomR + Het + HomD

	p := (HomR*(HomR-1) + 2*HomR*Het + 2*HomR*HomD + 0.75*Het*(Het-1) + Het*HomD) / (t * (t - 1))

	return p

}
