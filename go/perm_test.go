package bp

import "testing"

// TODO: func TestHeapAlgorithm(t *testing.T) {}

func TestRange(t *testing.T) {
	argumentBeg, argumentEnd, argumentStep := 1, 4, 1
	expected := []int{1, 2, 3}
	actual := Range(argumentBeg, argumentEnd, argumentStep)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%v', got:  '%v'", expected, actual)
		}
	}
}
