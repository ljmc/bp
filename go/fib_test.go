package bp

import "testing"

func TestFibonacci(t *testing.T) {
	argumentN, argumentK := 5, 3
	expected := 19
	actual := Fibonacci(argumentN, argumentK)

	if actual != expected {
		t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
	}
}
