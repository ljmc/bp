package bp

func BuildProfile(sequences []string) map[rune][]int {

	length := len(sequences[0])

	profile := map[rune][]int{
		'A': make([]int, length),
		'C': make([]int, length),
		'G': make([]int, length),
		'T': make([]int, length),
	}

	for _, sequence := range sequences {
		for i, v := range sequence {
			profile[v][i]++
		}
	}

	return profile
}

func BuildConsensus(profile map[rune][]int) string {

	var (
		score     int
		proposal  rune
		consensus string
	)

	length := len(profile['A'])

	for i := 0; i < length; i++ {
		score, proposal = 0, 0
		for j, v := range profile {
			if v[i] > score {
				score, proposal = v[i], j
			}
		}
		consensus += string(proposal)
	}

	return consensus
}
