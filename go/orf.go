package bp

func TranslateDNA(dnaSequence string) string {

	var peptideSequence string

	for i := 0; i+2 < len(dnaSequence); i = i + 3 {
		switch dnaSequence[i : i+3] {
		case "TTT", "TTC":
			peptideSequence += "F"
		case "TTA", "TTG", "CTT", "CTC", "CTA", "CTG":
			peptideSequence += "L"
		case "GCA", "GCC", "GCG", "GCT":
			peptideSequence += "A"
		case "ATT", "ATC", "ATA":
			peptideSequence += "I"
		case "ATG":
			peptideSequence += "M"
		case "GTT", "GTC", "GTA", "GTG":
			peptideSequence += "V"
		case "TCT", "TCC", "TCA", "TCG", "AGT", "AGC":
			peptideSequence += "S"
		case "CCT", "CCC", "CCA", "CCG":
			peptideSequence += "P"
		case "ACT", "ACC", "ACA", "ACG":
			peptideSequence += "T"
		case "TAT", "TAC":
			peptideSequence += "Y"
		case "CAT", "CAC":
			peptideSequence += "H"
		case "CAA", "CAG":
			peptideSequence += "Q"
		case "AAT", "AAC":
			peptideSequence += "N"
		case "AAA", "AAG":
			peptideSequence += "K"
		case "GAT", "GAC":
			peptideSequence += "D"
		case "GAA", "GAG":
			peptideSequence += "E"
		case "TGT", "TGC":
			peptideSequence += "C"
		case "TGG":
			peptideSequence += "W"
		case "CGT", "CGC", "CGA", "CGG", "AGA", "AGG":
			peptideSequence += "R"
		case "GGT", "GGC", "GGA", "GGG":
			peptideSequence += "G"
		case "TAA", "TAG", "TGA":
			break
		}
	}

	return peptideSequence
}

func FindORFs(dnaSequence string) []string {

	var (
		orfsmap map[string]bool = make(map[string]bool, 0)
		orfs    []string
		orf     string
	)

	rcSequence := ReverseComplement(dnaSequence)

	for _, strand := range []string{dnaSequence, rcSequence} {
		for i := 0; i+2 < len(strand); i++ {
			if strand[i:i+3] == "ATG" {
				for j := i; j+2 < len(strand); j += 3 {
					if strand[j:j+3] == "TAA" || strand[j:j+3] == "TAG" || strand[j:j+3] == "TGA" {
						orf = TranslateDNA(strand[i : j+3])
						break
					}
				}
				if _, ok := orfsmap[orf]; !ok {
					orfsmap[orf] = true
					orfs = append(orfs, orf)
				}
			}
		}
	}

	return orfs
}
