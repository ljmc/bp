package bp

import "strings"

func FindEdges(nodes map[string]string, k int) []string {

	var edges []string

	for originID, originNode := range nodes {

		suffix := originNode[len(originNode)-k:]

		for id, node := range nodes {
			if id != originID { // no self loop
				if strings.HasPrefix(node, suffix) {
					edges = append(edges, originID+" "+id)
				}
			}
		}
	}

	return edges
}
