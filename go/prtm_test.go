package bp

import "testing"

func TestProteinMass(t *testing.T) {
	argument := "SKADYEK"
	expected := 821.392
	actual := ProteinMass(argument)

	if !AlmostEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%f', got:  '%f'", expected, actual)
	}
}
