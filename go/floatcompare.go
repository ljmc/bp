package bp

import "math"

const float64EqualityThreshold = 1e-3

func AlmostEqual(a, b float64) bool {
	return math.Abs(a-b) <= float64EqualityThreshold
}
