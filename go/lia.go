package bp

// package main

// import (
// 	"fmt"
// 	"io/ioutil"
// 	"log"
// 	"math"
// 	"strconv"
// 	"strings"
// )

// func factorial(n float64) float64 {
// 	switch {
// 	case math.IsInf(n, 1):
// 		return n
// 	case n > 0:
// 		return n * factorial(n-1)
// 	case n == 0:
// 		return 1.
// 	default:
// 		return math.NaN()
// 	}
// }

// func nCk(n, k float64) (float64, error) {
// 	if n < 0. || k < 0 || n < k {
// 		return math.NaN(), fmt.Errorf("Arguments need to respect n >= k >= 0")
// 	}
// 	return factorial(n) / factorial(k) / factorial(n-k), nil
// }

// func cumulativeBinomial(j, k, n, p float64) (float64, error) {
// 	pr := 0.
// 	for i := j; i <= k; i++ {
// 		bc, err := nCk(n, i)
// 		if err != nil {
// 			return math.NaN(), fmt.Errorf("nCk failed: %w", err)
// 		}
// 		pr += bc * math.Pow(p, i) * math.Pow(1-p, n-i)
// 	}
// 	return pr, nil
// }

// func main() {

// 	// if len(os.Args) < 2 {
// 	// 	log.Fatal("Please input filename as argument")
// 	// }

// 	// inputData, err := ioutil.ReadFile(os.Args[1])
// 	inputData, err := ioutil.ReadFile("./dataset.txt")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	data := strings.Fields(string(inputData))

// 	k, err := strconv.Atoi(data[0])
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	N, err := strconv.Atoi(data[1])
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	n := math.Pow(2, float64(k))
// 	p := 0.25

// 	pr, err := cumulativeBinomial(float64(N), n, n, p)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	fmt.Println(pr)
// }
