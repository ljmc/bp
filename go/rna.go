package bp

func Transcribe(dnaSequence string) string {

	var rnaSequence string

	for _, v := range dnaSequence {
		if v == 'T' {
			rnaSequence += "U"
		} else {
			rnaSequence += string(v)
		}
	}

	return rnaSequence
}
