package bp

import "fmt"

func MutationRatio(seqA, seqB string) (float64, error) {

	var transitions, transversions float64

	if len(seqA) != len(seqB) {
		return 0.0, fmt.Errorf("Please input sequences of identical length")
	}

	for i := 0; i < len(seqA); i++ {
		if seqA[i] != seqB[i] {
			if seqA[i] == 'A' && seqB[i] == 'G' ||
				seqA[i] == 'G' && seqB[i] == 'A' ||
				seqA[i] == 'C' && seqB[i] == 'T' ||
				seqA[i] == 'T' && seqB[i] == 'C' {
				transitions++
			} else {
				transversions++
			}
		}
	}

	return transitions / transversions, nil
}
