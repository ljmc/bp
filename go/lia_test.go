package bp

// package main

// import (
// 	"math"
// 	"testing"
// )

// func tolerance(a, b, e float64) bool {
// 	// note: b is correct (expected) value, a is actual value.
// 	// make error tolerance a fraction of b, not a.
// 	if a == b {
// 		return true
// 	}
// 	d := a - b
// 	if d < 0 {
// 		d = -d
// 	}
// 	if b != 0 {
// 		e = e * b
// 		if e < 0 {
// 			e = -e
// 		}
// 	}
// 	return d < e
// }

// const eps32 float64 = float64(float32(7.)/3 - float32(4.)/3 - float32(1.))
// const eps64 float64 = float64(7.)/3 - float64(4.)/3 - float64(1.)

// func isApprox(a, b float64) bool { return tolerance(a, b, eps32) }

// func TestFactorial(t *testing.T) {
// 	t.Parallel()
// 	tests := []struct {
// 		name string
// 		in   float64
// 		out  float64
// 	}{
// 		{"-Inf", math.Inf(-1), math.Inf(-1)},
// 		{"-1", -1., math.NaN()},
// 		{"-0", math.Copysign(0., -1), 1.},
// 		{"0", 0., 1.},
// 		{"1", 1., 1.},
// 		{"5", 5., 120.},
// 		{"+Inf", math.Inf(1), math.Inf(1)},
// 		{"NaN", math.NaN(), math.NaN()},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			res := factorial(test.in)
// 			if test.in >= 0 || math.IsInf(test.in, 1) { // positive or infinites
// 				if res != test.out {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			} else { // negative or Nan
// 				if !(res != res) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			}
// 		})
// 	}
// }

// func TestBinomialCoef(t *testing.T) {
// 	t.Parallel()
// 	tests := []struct {
// 		name string
// 		n    float64
// 		k    float64
// 		out  float64
// 	}{
// 		{"-Inf,1", math.Inf(-1), 1., math.NaN()},
// 		{"-1,-1", -1., -1., math.NaN()},
// 		{"-0,0", math.Copysign(0., -1), 0., 1.},
// 		{"0,0", 0., 0., 1.},
// 		{"1,1", 1., 1., 1.},
// 		{"12,7", 12., 7., 792.},
// 		{"+Inf,1", math.Inf(1), 1., math.NaN()},
// 		{"NaN,1.", math.NaN(), 1., math.NaN()},
// 		{"7,12", 7., 12., math.NaN()},
// 		{"12,-7", 12., -7., math.NaN()},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			res, _ := nCk(test.n, test.k)
// 			if !math.IsInf(test.n, 1) && test.n >= 0 && test.k >= 0 && test.n >= test.k { // respect n >= k >= 0
// 				if res != test.out {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			} else { // rest
// 				if !(res != res) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			}
// 		})
// 	}
// }

// func TestCumulativeBinomial(t *testing.T) {
// 	t.Parallel()
// 	tests := []struct {
// 		name string
// 		j    float64
// 		k    float64
// 		n    float64
// 		p    float64
// 		out  float64
// 	}{
// 		{"-1,-1,24,1/3", -1, -1., 24., 1. / 3, math.NaN()},
// 		{"-1,4,24,1/3", -1, 4., 24., 1. / 3, math.NaN()},
// 		{"-0,4,24,1/3", math.Copysign(0, -1), 4., 24., 1. / 3, 0.05935121427049},
// 		{"0,4,24,1/3", 0., 4., 24., 1. / 3, 0.05935121427049},
// 		{"0,4,24,0", 0., 4., 24., 0., 1.},
// 		{"0,4,24,1", 0., 4., 24., 1., 0.},
// 		{"4,24,24,1/3", 4., 24., 24., 1. / 3, 0.98009993065871},
// 		{"4,2,24,1/3", 4., 2., 24., 1. / 3, 0.},
// 		{"0,24,4,1/3", 0., 4., 2., 1. / 3, math.NaN()},
// 		{"NaN,4,24,1/3", math.NaN(), 4., 24., 1. / 3, 0.},
// 		{"-Inf,4,24,1/3", math.Inf(-1), 4., 24., 1. / 3, math.NaN()},
// 		{"+Inf,4,24,1/3", math.Inf(1), 4., 24., 1. / 3, 0.},
// 		{"0,NaN,24,1/3", 0., math.NaN(), 24., 1. / 3, 0.},
// 		{"0,-Inf,24,1/3", 0., math.Inf(-1), 24., 1. / 3, 0.},
// 		{"0,+Inf,24,1/3", 0., math.Inf(1), 24., 1. / 3, math.NaN()},
// 		{"0,4,NaN,1/3", 0., 4., math.NaN(), 1. / 3, math.NaN()},
// 		{"0,4,-Inf,1/3", 0., 4., math.Inf(-1), 1. / 3, math.NaN()},
// 		{"0,4,+Inf,1/3", 0., 4., math.Inf(1), 1. / 3, math.NaN()},
// 		{"0,4,24,NaN", 0., 4., 24., math.NaN(), math.NaN()},
// 		{"0,4,24,-Inf", 0., 4., 24., math.Inf(-1), math.NaN()},
// 		{"0,4,24,+Inf", 0., 4., 24., math.Inf(1), math.NaN()},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			res, _ := cumulativeBinomial(test.j, test.k, test.n, test.p)
// 			switch {
// 			case test.j < 0 || math.IsInf(test.j, -1):
// 				if !(res != res) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			case test.k > test.n || math.IsInf(test.k, 1):
// 				if !(res != res) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			case math.IsNaN(test.n) || math.IsInf(test.n, 0):
// 				if !(res != res) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			case math.IsNaN(test.p) || math.IsInf(test.p, 0):
// 				if !(res != res) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			default:
// 				if !isApprox(res, test.out) {
// 					t.Errorf("Test %v failed, expected: '%v', got: '%v'", test.name, test.out, res)
// 				}
// 			}
// 		})
// 	}
// }
