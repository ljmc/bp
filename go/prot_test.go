package bp

import "testing"

func TestTranslateRNA(t *testing.T) {
	argument := "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA"
	expected := "MAMAPRTEINSTRING"
	actual := TranslateRNA(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}

	// original sample does not check all amino acids
	argument = "GCAUGUGAUGAAUUUGGUCAUAUUAAAUUAAUGAAUCCUCAACGUUCUACUGUUUGGUAUUAA"
	expected = "ACDEFGHIKLMNPQRSTVWY"
	actual = TranslateRNA(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
