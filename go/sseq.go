package bp

import "strings"

func SubsequenceIndexes(sequence, subsequence string) []int {

	var i, j int
	var indexes []int

	for i < len(sequence) && j < len(subsequence) {
		if indexes == nil {
			i = strings.Index(sequence[i:], string(subsequence[j])) + 1
		} else {
			i = strings.Index(sequence[i:], string(subsequence[j])) + 1 + indexes[len(indexes)-1]
		}
		indexes = append(indexes, i)
		j++
	}

	return indexes
}
