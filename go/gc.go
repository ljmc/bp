package bp

import (
	"bufio"
	"bytes"
	"strings"
)

func ParseFasta(inputFasta []byte) map[string]string {

	var entries = make(map[string]string)
	var id string

	scanner := bufio.NewScanner(bytes.NewReader(inputFasta))

	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), ">") {
			id = strings.TrimLeft(scanner.Text(), ">")
			entries[id] = ""
		} else {
			entries[id] += scanner.Text()
		}
	}

	return entries
}

func GCContent(sequence string) float64 {

	return float64(strings.Count(sequence, "C")+strings.Count(sequence, "G")) * 100.0 / float64(len(sequence))
}

func GCContentHighest(sequences map[string]string) (string, float64) {

	var maxGCId string
	var maxGC float64

	for k, v := range sequences {
		if gc := GCContent(v); gc >= maxGC {
			maxGCId, maxGC = k, gc
		}
	}

	return maxGCId, maxGC
}
