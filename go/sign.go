package bp

func SignPermutations(n int) [][]int {

	var (
		array    []int = make([]int, n)
		arrays   [][]int
		generate func(n, i int, arr []int)
	)

	generate = func(n, i int, arr []int) {
		if i == n {
			tmp := make([]int, n)
			copy(tmp, arr)
			arrays = append(arrays, tmp)
			return
		}
		array[i] = -1
		generate(n, i+1, arr)
		array[i] = 1
		generate(n, i+1, arr)
	}

	generate(n, 0, array)

	return arrays
}

func HeapAlgorithmSigned(array []int) [][]int {

	n := len(array)
	perms := HeapAlgorithm(array)
	signs := SignPermutations(n)

	var signedperms [][]int

	for _, perm := range perms {
		for _, sign := range signs {
			signedperm := make([]int, n)
			for i := 0; i < n; i++ {
				signedperm[i] = perm[i] * sign[i]
			}
			signedperms = append(signedperms, signedperm)
		}
	}

	return signedperms
}
