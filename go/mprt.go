package bp

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
)

const (
	nGlycosylation string = "N[^P][ST][^P]"
)

func FetchUniprotFasta(id string) []byte {

	url := "http://www.uniprot.org/uniprot/" + id + ".fasta"

	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return body
}

func ReadFasta(inputFasta []byte) (string, string) {

	var id, sequence string

	scanner := bufio.NewScanner(bytes.NewReader(inputFasta))

	for scanner.Scan() {
		if id == "" {
			id = strings.TrimLeft(scanner.Text(), ">")
		} else {
			sequence += scanner.Text()
		}
	}

	return id, sequence
}

func FindMotifsRegexp(sequence, motif string) []int { // FIXME: not all motifs are found

	var indexes []int
	var i int

	re := regexp.MustCompile(motif)

	for re.MatchString(sequence[i:]) {
		if indexes == nil {
			i = re.FindStringIndex(sequence)[0] + 1
		} else {
			i = re.FindStringIndex(sequence[i:])[0] + 1 + indexes[len(indexes)-1]
		}
		indexes = append(indexes, i)
	}

	return indexes
}
