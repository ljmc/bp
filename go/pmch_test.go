package bp

import (
	"math/big"
	"testing"
)

func TestCountRNANucleotides(t *testing.T) {
	argument := "AGCUAGUCAU"
	expected := []int{3, 2, 2, 3}
	actual := CountRNANucleotides(argument)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
		}
	}
}

func TestCountPerfectMatchings(t *testing.T) {
	argument := "AGCUAGUCAU"
	expected := big.NewInt(12)
	actual, err := CountPerfectMatchings(argument)

	if err != nil {
		t.Errorf("Test failed, error found '%s'", err)
	}

	if actual.Cmp(expected) != 0 {
		t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
	}
}
