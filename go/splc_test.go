package bp

import "testing"

func TestSpliceIntrons(t *testing.T) {
	argumentSeq := "ATGGTCTACATAGCTGACAAACAGCACGTAGCAATCGGTCGAATCTCGAGAGGCATATGGTCACATGATCGGTCGAGCGTGTTTCAAAGTTTGCGCCTAG"
	argumentIntrons := []string{"ATCGGTCGAA", "ATCGGTCGAGCGTGT"}
	expected := "ATGGTCTACATAGCTGACAAACAGCACGTAGCATCTCGAGAGGCATATGGTCACATGTTCAAAGTTTGCGCCTAG"
	actual := SpliceIntrons(argumentSeq, argumentIntrons...)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}

func TestSpliceTranslateDNA(t *testing.T) {
	argumentSeq := "ATGGTCTACATAGCTGACAAACAGCACGTAGCAATCGGTCGAATCTCGAGAGGCATATGGTCACATGATCGGTCGAGCGTGTTTCAAAGTTTGCGCCTAG"
	argumentIntrons := []string{"ATCGGTCGAA", "ATCGGTCGAGCGTGT"}
	expected := "MVYIADKQHVASREAYGHMFKVCA"
	actual := SpliceTranslateDNA(argumentSeq, argumentIntrons...)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
