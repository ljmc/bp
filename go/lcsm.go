package bp

import "strings"

func FindCommonMotif(sequences []string) string { // OPTIMIZE: slow search right now

	var motif, kmer string
	var score int

	origin := sequences[0]

	for k := len(origin) - 1; k > 1; k-- {
		for i := 0; i <= len(origin)-k; i++ {
			kmer = origin[i : i+k]
			for _, sequence := range sequences[1:] {
				if !strings.Contains(sequence, kmer) {
					break
				}
				if len(kmer) >= score {
					motif = kmer
					score = len(kmer)
				}
			}
		}
	}

	return motif
}
