package bp

import (
	"sort"
	"testing"
)

func TestTranslateDNA(t *testing.T) {
	argument := "GCATGTGATGAATTTGGTCATATTAAATTAATGAATCCTCAACGTTCTACTGTTTGGTATTAA"
	expected := "ACDEFGHIKLMNPQRSTVWY"
	actual := TranslateDNA(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}

func TestFindORFs(t *testing.T) {
	argument := "AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGATTAGAGTCTCTTTTGGAATAAGCCTGAATGATCCGAGTAGCATCTCAG"
	expected := []string{
		"M",
		"MGMTPRLGLESLLE",
		"MLLGSFRLIPKETLIQVAGSSPCNLS",
		"MTPRLGLESLLE",
	}
	actual := FindORFs(argument)
	sort.Strings(actual)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%s', got:  '%s'", expected[i], actual[i])
		}
	}
}
