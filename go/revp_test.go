package bp

import "testing"

func TestPalindromesFindIndexes(t *testing.T) {
	argumentSequence := "TCAATGCATGCGGGTCTATATGCAT"
	argumentLengths := Range(4, 14, 2)
	expected := [][2]int{
		{5, 4},
		{7, 4},
		{17, 4},
		{18, 4},
		{21, 4},
		{4, 6},
		{6, 6},
		{20, 6},
	}
	actual := PalindromesFindIndexes(argumentSequence, argumentLengths)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%v', got:  '%v'", expected[i], actual[i])
		}
	}
}
