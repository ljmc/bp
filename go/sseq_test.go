package bp

import "testing"

func TestSubsequenceIndexes(t *testing.T) {
	argumentSequence := "ACGTACGTGACG"
	argumentSubsequence := "GTA"
	expected := []int{3, 4, 5}
	actual := SubsequenceIndexes(argumentSequence, argumentSubsequence)

	for i := range actual {
		if expected[i] != actual[i] {
			t.Errorf("Test failed, expected: '%v', got:  '%v'", expected, actual)
		}
	}
}
