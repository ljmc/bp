package bp

import "strings"

func AssembleReads(reads []string) string {

	var assemble func(reads []string) []string

	assemble = func(reads []string) []string {
		var overlap int
		var proposal string
		var proposals = make(map[string]bool)

		for _, origin := range reads {
			overlap = int(len(origin) / 2)
			for _, read := range reads {
				if read != origin { // no self loop
					for i := int(len(origin) / 2); i <= len(origin); i++ {
						if strings.HasPrefix(read, origin[len(origin)-i:]) && i > overlap {
							proposal = origin + read[i:]
							overlap = i
						}
					}
				}
			}
			if _, ok := proposals[proposal]; !ok && proposal != "" { // no empty proposals
				proposals[proposal] = true
			}
		}

		var contigs = make([]string, len(proposals))

		i := 0
		for contig := range proposals {
			contigs[i] = contig
			i++
		}

		return contigs
	}

	for len(reads) > 1 {
		reads = assemble(reads)
	}

	return reads[0]
}
