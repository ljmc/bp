package bp

import "strings"

func SpliceIntrons(sequence string, introns ...string) string {

	for _, intron := range introns {
		i := strings.Index(sequence, intron)
		sequence = sequence[:i] + sequence[i+len(intron):]
	}

	return sequence
}

func SpliceTranslateDNA(sequence string, introns ...string) string {
	return TranslateDNA(SpliceIntrons(sequence, introns...))
}
