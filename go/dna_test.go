package bp

import "testing"

func TestCountDNANucleotides(t *testing.T) {
	argument := "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"
	expected := []int{20, 12, 17, 21}
	actual := CountDNANucleotides(argument)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
		}
	}
}
