package bp

import "testing"

func TestBuildProfile(t *testing.T) {
	argument := []string{
		"ATCCAGCT",
		"GGGCAACT",
		"ATGGATCT",
		"AAGCAACC",
		"TTGGAACT",
		"ATGCCATT",
		"ATGGCACT",
	}
	expected := map[rune][]int{
		'A': {5, 1, 0, 0, 5, 5, 0, 0},
		'C': {0, 0, 1, 4, 2, 0, 6, 1},
		'G': {1, 1, 6, 3, 0, 1, 0, 0},
		'T': {1, 5, 0, 0, 0, 1, 1, 6},
	}
	actual := BuildProfile(argument)

	for iA, vA := range actual {
		vE, ok := expected[iA]
		if !ok {
			t.Errorf("Test failed, index '%c', not found.", iA)
		} else {
			for i := range vA {
				if vE[i] != vA[i] {
					t.Errorf("Test failed, expected: '%d', got:  '%d'", vE, vA)
				}
			}
		}
	}
}

func TestBuildConsensus(t *testing.T) {
	argument := map[rune][]int{
		'A': {5, 1, 0, 0, 5, 5, 0, 0},
		'C': {0, 0, 1, 4, 2, 0, 6, 1},
		'G': {1, 1, 6, 3, 0, 1, 0, 0},
		'T': {1, 5, 0, 0, 0, 1, 1, 6},
	}
	expected := "ATGCAACT"
	actual := BuildConsensus(argument)

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
