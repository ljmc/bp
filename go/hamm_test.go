package bp

import "testing"

func TestHammingDistance(t *testing.T) {
	argumentA, argumentB := []byte("GAGCCTACTAACGGGAT"), []byte("CATCGTAATGACGGCCT")
	expected := 7
	actual, err := HammingDistance(argumentA, argumentB)

	if err != nil {
		t.Errorf("Test failed, error found '%s'", err)
	}

	if actual != expected {
		t.Errorf("Test failed, expected: '%d', got:  '%d'", expected, actual)
	}
}
