package bp

import "testing"

func TestAssembleReads(t *testing.T) {
	argument := []string{
		"ATTAGACCTG",
		"CCTGCCGGAA",
		"AGACCTGCCG",
		"GCCGGAATAC",
	}
	expected := "ATTAGACCTGCCGGAATAC"
	actual := AssembleReads(argument)

	if expected != actual {
		t.Errorf("Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}
