NB. print longer lines
(9!:37) 0 16384 0 222

NB. DNA
input =: 'AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC'
output =: 20 12 17 21
counts1 =: +/@:(=/)~
NB. (10^6) (6!:2) '''ACGT'' counts1 input'  NB. 8.064e_7
counts2 =: [:+/=/~
NB. (10^6) (6!:2) '''ACGT'' counts2 input'  NB. 6.6906e_7
counts3 =: 4 : '<:(#x){.#/.~ x,y'
NB. (10^6) (6!:2) '''ACGT'' counts3 input'  NB. 6.3295e_7
counts4 =: [:+/"1=/  NB. cap sum in rank 1 of outer product equal
NB. (10^6) (6!:2) '''ACGT'' counts4 input'  NB. 3.885e_7
counts5 =: (([:#/.~]) /: (/:~.))  NB. count partitions of y in nub order and order by nub order of x
NB. (10^6) (6!:2) '''ACGT'' counts5 input'  NB. 9.1569e_7
counts =: counts4
output -: 'ACGT' counts input

NB. RNA
input =: 'GATGGAACTTGACTACGTAAATT'
output =: 'GAUGGAACUUGACUACGUAAAUU'
findTs1 =: 3 : '(''T''&=y)#(i.#y)'
NB. (10^6) (6!:2) 'findTs1 input'  NB. 4.57393e_7
findTs2 =: 3 : '''T''(I.@:=)y'
NB. (10^6) (6!:2) 'findTs2 input'  NB. 3.66177e_7
findTs3 =: [:'T'&(I.@:=)]
NB. (10^6) (6!:2) 'findTs3 input'  NB. 3.19174e_7
findTs4 =: [:'T'&([:I.=)]
NB. (10^6) (6!:2) 'findTs4 input'  NB. 3.19663e_7
findTs5 =: [:([:I.'T'&=)]
NB. (10^6) (6!:2) 'findTs5 input'  NB. 3.09554e_7
findTs6 =: I.@('T'&=)
NB. (10^6) (6!:2) 'findTs6 input'  NB. 2.9713e_7
findTs7 =: [: I. 'T'=]
NB. (10^6) (6!:2) 'findTs7 input'  NB. 3.18046e_7
findTs =: findTs7
transcribe1 =: 'U'&(findTs })
NB. (10^6) (6!:2) 'transcribe1 input'  NB. 4.59781e_7
transcribe2 =: 'TU' & charsub
NB. (10^6) (6!:2) 'transcribe2 input'  NB. 1.18785e_6
transcribe3 =: rplc & ('T';'U')
NB. (10^6) (6!:2) 'transcribe3 input'  NB. 3.67063e_6
transcribe4 =: 'U'&(I.@:([:'T'&=])})
NB. (10^6) (6!:2) 'transcribe4 input'  NB. 4.26313e_7
transcribe5 =: 'U' ( [: I. 'T'=] ) } ]
NB. (10^6) (6!:2) 'transcribe5 input'  NB. 4.32261e_7
transcribe6 =: 3 : 0
ts =. I. 'T'=y
'U' ts } y
)
NB. (10^6) (6!:2) 'transcribe6 input'  NB. 5.29171e_7
transcribe =: transcribe5
output -: transcribe input

NB. REVC
input =: 'AAAACCCGGT'
output =: 'ACCGGGTTTT'
reverseComplement1 =: 3 : '(|. ''ACGT'' i. y) { ''TGCA'' '
NB. (10^6) (6!:2) 'reverseComplement1 input'  NB. 4.38991e_7
reverseComplement2 =: 'ACGT' ( ([:|.[) {~ (i.|.) ) ]
NB. (10^6) (6!:2) 'reverseComplement2 input'  NB. 4.11538e_7
reverseComplement =: reverseComplement2
output -: reverseComplement input

NB. SUBS
input =: 'GATATATGCATATACTT',LF,'ATAT'
output =: 2 4 10
findSubstrings1 =: 4 : '>: I. (#x) = (#x) ((+/"1) @ (x&=)) \ y'
NB. (10^6) (6!:2) '''ATAT'' findSubstrings1 ''GATATATGCATATACTT'''  NB. 1.69659e_6
findSubstrings2 =: 4 : '>: I. -. (<x) i. ((#x) <\ y)'
NB. (10^6) (6!:2) '''ATAT'' findSubstrings2 ''GATATATGCATATACTT'''  NB. 9.80355e_7
findSubstrings =: findSubstrings2
tidy =: CR -.~ ] , LF -. {:
'target substring' =: <;._2 tidy input
output -: substring findSubstrings target

NB. GC
input =: 0 : 0
>Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT
)
output =: 'Rosalind_0808' ; 60.9195
parseFasta =: 3 : 0
y =. tidy y ,~ '>' -. {. y  NB. ensure y starts with '>' and tidy
((LF&taketo) ; (LF -.~ LF&takeafter)) ;._1 y  NB. split desc and cleaned seq
)
gcContent =: (+/ % #) @ (e.&'GC')
records =: parseFasta input
records =: records ,. ((100&*@gcContent) each 1{"1 records)
records =: records \: 2{"1 records
output -: 0 2 { {. records  NB. why does this not work ?

NB. HAMM
input =: 0 : 0
GAGCCTACTAACGGGAT
CATCGTAATGACGGCCT
)
output =: 7
hammingDistance1 =: {{ +/ -. x = y }}
NB. (11^6) (6!:2) '''GAGCCTACTAACGGGAT'' hammingDistance1 ''CATCGTAATGACGGCCT'''  NB. 5.66394e_7
hammingDistance2 =: [: +/ [: -. =
NB. (10^6) (6!:2) '''GAGCCTACTAACGGGAT'' hammingDistance2 ''CATCGTAATGACGGCCT'''  NB. 4.29363e_7
hammingDistance3 =: [: +/ -.@=
NB. (10^6) (6!:2) '''GAGCCTACTAACGGGAT'' hammingDistance3 ''CATCGTAATGACGGCCT'''  NB. 1.37752e_6
hammingDistance4 =: +/@:-.@:=
NB. (10^6) (6!:2) '''GAGCCTACTAACGGGAT'' hammingDistance4 ''CATCGTAATGACGGCCT'''  NB. 4.42856e_7
hammingDistance =: hammingDistance2
output -: hammingDistance4 / ;;._2 tidy input

NB. IPRB
input =: '2 2 2'
output =: 0.78333
'k m n' =: ". input
t =: k + m + n
p =: ((k*(k-1)) + (2*k*m) + (2*k*n) + (0.75*m*(m-1)) + (m*n)) % (t * (t-1))
output -: p

NB. IEV
input =: '1 0 0 1 0 1'
output =: 3.5
couples =: ". input
passDominant =: 1 1 1 0.75 0.5 0
p =: +/ 2 * couples * passDominant
output -: p

NB. PROT
input =: 'AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA'
output =: 'MAMAPRTEINSTRING'
geneticCodeTable =: 0 : 0
UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G
)
stopMarker =: <,'Stop'
parseGeneticCodeTable =: _2 ]\ [: cut (LF;' ') rplc~ tidy
geneticCode =: parseGeneticCodeTable geneticCodeTable
translateCodon =: (1{"1[) {~ (0{"1[) i. ]
translateORF =: [: ; [: }: [ translateCodon _3<\ ]
takeToStopMarker =: {.~ (i.&stopMarker)
translateSequence =: takeToStopMarker @: [ translateCodon _3<\ ]
output -: geneticCode translateORF input
